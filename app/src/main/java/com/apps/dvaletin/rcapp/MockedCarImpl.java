package com.apps.dvaletin.rcapp;/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Gnu Public License, Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/gpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import android.util.Log;

public class MockedCarImpl implements Car {
    private static final String TAG = MockedCarImpl.class.getSimpleName();

    @Override
    public void stop() {
        Log.d(TAG, "stop");
    }

    @Override
    public void back() {
        Log.d(TAG, "back");
    }

    @Override
    public void forward() {
        Log.d(TAG, "forward");
    }

    @Override
    public void left() {
        Log.d(TAG, "left");
    }

    @Override
    public void right() {
        Log.d(TAG, "right");
    }

    @Override
    public void backLeft() {
        Log.d(TAG, "back-left");
    }

    @Override
    public void backRight() {
        Log.d(TAG, "back-right");
    }

    @Override
    public void forwardLeft() {
        Log.d(TAG, "forward-left");
    }

    @Override
    public void forwardRight() {
        Log.d(TAG, "forward-right");
    }
}
