package com.apps.dvaletin.rcapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.apps.dvaletin.rcapp.view.JoystickView;


public class RcActivity extends AppCompatActivity implements View.OnClickListener {

    private Car car;
    private int currentDirection = JoystickView.DIRECTION_CENTER;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rc);
        car = new CarImpl();
//        car = new MockedCarImpl();
    }

    @Override
    public void onClick(final View view) {
        Runnable r = new Runnable() {

            @Override
            public void run() {

                switch (view.getId()) {
                    case R.id.btnBack: {
                        car.back();
                        break;
                    }
                    case R.id.btnUp: {
                        car.forward();
                        break;
                    }
                    case R.id.btnLeft: {
                        car.left();
                        break;
                    }
                    case R.id.btnRight: {
                        car.right();
                        break;
                    }
                    case JoystickView.DIRECTION_DOWN_LEFT: {
                        car.backLeft();
                        break;
                    }
                    case JoystickView.DIRECTION_RIGHT_DOWN: {
                        car.backRight();
                        break;
                    }
                    case JoystickView.DIRECTION_LEFT_UP: {
                        car.forwardLeft();
                        break;
                    }
                    case JoystickView.DIRECTION_UP_RIGHT: {
                        car.forwardRight();
                        break;
                    }
                    case R.id.btnStop:
                    default:
                        car.stop();
                }

            }
        };
        new Thread(r).start();
    }
}
