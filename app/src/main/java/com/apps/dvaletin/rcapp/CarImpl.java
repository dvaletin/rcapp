package com.apps.dvaletin.rcapp;/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Gnu Public License, Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/gpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import com.apps.dvaletin.rcapp.server.ServerEndpoints;

import java.io.IOException;

import retrofit2.Retrofit;

public class CarImpl implements Car {

    ServerEndpoints endpoints;

    public CarImpl() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://192.168.1.45")
                .build();
        endpoints = retrofit.create(ServerEndpoints.class);
    }

    @Override
    public void stop() {
        try {
            endpoints.stop().execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void back() {
        try {
            endpoints.back().execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void forward() {
        try {
            endpoints.forward().execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void left() {
        try {
            endpoints.left().execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void right() {
        try {
            endpoints.right().execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void backLeft() {
        try {
            endpoints.backLeft().execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void backRight() {
        try {
            endpoints.backRight().execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void forwardLeft() {
        try {
            endpoints.forwardLeft().execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void forwardRight() {
        try {
            endpoints.forwardRight().execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
