package com.apps.dvaletin.rcapp.server;
/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Gnu Public License, Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/gpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import retrofit2.Call;
import retrofit2.http.GET;

public interface ServerEndpoints {

    @GET("/back")
    Call<Void> back();

    @GET("/forward")
    Call<Void> forward();

    @GET("/left")
    Call<Void> left();

    @GET("/right")
    Call<Void> right();

    @GET("/stop")
    Call<Void> stop();

    @GET("/back-left")
    Call<Void> backLeft();

    @GET("/back-right")
    Call<Void> backRight();

    @GET("/forward-left")
    Call<Void> forwardLeft();

    @GET("/forward-right")
    Call<Void> forwardRight();
}
