# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Android app to control NodeMCU-based robot
* 1.0

### How do I get set up? ###

* Install Android Studio from https://developer.android.com/studio/index.html
* Checkout project
* Edit RcActivity.java and replace BASE_NODE_MCU_URL constant with IP of your NodeMCU
* Compile and run on any Android device

### Who do I talk to? ###

* Repo owner - https://www.facebook.com/dmitry.valetin